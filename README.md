# Task23



## This is a task

Required Points:
1. Create a user table with its CRUD.
2. Create Permissions and Roles CRUD.
3. Allow the admin to assign a role and its permissions to any user.
4. Do not allow any user to delete the very first user or even edit its data.
5. Only user (1) can edit himself.
6. Create a route prefix for the Administrator and for the Client to be separated after login.
7. Redirect after login to which Panel based on role.
8. User details: (Name - Phone – E-mail - Password - Photo).
9. Can login with phone or e-mail.
10. Every user can view and edit his profile, and change his data.
11. Languages (Arabic - English).
12. Not allowed to use any packages to do any point of the above.
13. Livewire is preferred but not a must (you have to show all your skills).
14. Complete your task in only 3 days starting from (05/06/2023) till (08/06/2023) and then upload it to GitHub
    with the database included.

## Discription

This website to the admin dashboard and clients in two languages Arabic and English with login by email and phone
an admin can make a role from permission to know what the user can show on the website

```
TO SETUP THE PROJECT
composer install 
copy .env.example .env
PHP artisan key:generate
PHP artisan storage:link
PHP artisan migrate:fresh --seed
```

## Suggestions

- we can use a datatable package to handle it in the index blade for more features
- for images, we can use a spatie media to handle it and more flexible
