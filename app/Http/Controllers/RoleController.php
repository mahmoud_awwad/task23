<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if(!auth()->user()->hasPermission('permission_index') && auth()->user()->id != "1"){
             abort(404);
        }
        $roles = Role::all();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if(!auth()->user()->hasPermission('permission_create') && auth()->user()->id != "1"){
            abort(404);
        }
        $permissions = $permissions = Permission::select('model_name')->distinct()->pluck('model_name');
        return view('roles.create',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|unique:roles,name',
            'permissions' => 'array',
        ]);
        $role = Role::create($validate);
        $role->permissions()->sync($validate['permissions']);
        return redirect()->route('Roles.index')->with('success', 'Role created successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $Role)
    {
        if(!auth()->user()->hasPermission('permission_edit') && auth()->user()->id != "1"){
            abort(404);
        }
        $permissions = $permissions = Permission::select('model_name')->distinct()->pluck('model_name');
        return view('roles.edit',compact('Role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $Role)
    {
        $validate =$request->validate([
            'name' => 'required|unique:roles,name,' . $Role->id,
            'permissions' => 'array',
        ]);
        $Role->update($validate);
        $Role->permissions()->sync($validate['permissions']);
        return redirect()->route('Roles.index')->with('success', 'Role created successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $Role)
    {
        $Role->delete();
        return redirect()->route('Roles.index')->with('success', 'Role deleted successfully!');
    }
}
