<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class UserController extends Controller
{
    public function index()
    {
        if(!auth()->user()->hasPermission('user_index') && auth()->user()->id != "1"){
            abort(404);
        }
        $users = User::HideMe()->get();
        return view('users.index',compact('users'));
    }

    public function create()
    {
        if(!auth()->user()->hasPermission('user_create') && auth()->user()->id != "1"){
            abort(404);
        }
        $roles = Role::all();
        return view('users.create',compact('roles'));
    }

    public function store(CreateUserRequest  $request)
    {
        $validated = $request->validated();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imagePath = $image->store('images', 'public');
            $validated['image'] = $imagePath;
        }

        User::create($validated);

        return redirect()->route('users.index')->with('success', 'User created successfully!');
    }

    public function edit(User $user)
    {
        if(!auth()->user()->hasPermission('user_edit') && auth()->user()->id != "1"){
            abort(404);
        }
        $roles = Role::all();
        return view('users.edit',compact('user','roles'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($user->image){
                Storage::disk('public')->delete($user->image);
            }
            $image = $request->file('image');
            $imagePath = $image->store('images', 'public');
            $validated['image'] = $imagePath;
        }
        $user->update($validated);
        return redirect()->route('users.index')->with('success', 'User updated successfully!');
    }

    public function destroy(User $user)
    {
        if ($user->image){
            Storage::disk('public')->delete($user->image);
        }
        $user->delete();
        return redirect()->route('users.index')->with('success', 'User deleted successfully!');
    }

    public function user_profile()
    {
        return view('users.profile');
    }

    public function update_user_profile(UpdateUserRequest $request, User $user)
    {
        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($user->image){
                Storage::disk('public')->delete($user->image);
            }
            $image = $request->file('image');
            $imagePath = $image->store('images', 'public');
            $validated['image'] = $imagePath;
        }
        $user->update($validated);
        return view('users.profile',compact('user'));
    }

}
