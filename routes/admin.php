<?php

use App\Http\Controllers\ChangeLocalizationAction;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\LocalizationMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([ 'middleware' => LocalizationMiddleware::class], function () {

    Route::resource('users', UserController::class);
    Route::resource('Roles', RoleController::class);
//    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
