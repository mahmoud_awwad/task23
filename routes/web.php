<?php

use App\Http\Controllers\ChangeLocalizationAction;
use App\Http\Controllers\UserController;
use App\Http\Middleware\LocalizationMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('change-language/{locale}', [ChangeLocalizationAction::class, '__invoke'])
    ->name('change-language');

Route::group([ 'middleware' => LocalizationMiddleware::class], function () {
        // Routes for administrators
    Route::prefix('admin')->middleware('admin')->group(function () {
        require __DIR__ . '/admin.php';
    });

    // Routes for clients
    Route::middleware('auth')->group(function () {
        require __DIR__ . '/user.php';
    });

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class,'logout']);
    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/show-profile', [App\Http\Controllers\UserController::class, 'user_profile'])->name('show_profile');
    Route::PUT('/update-profile/{user}', [App\Http\Controllers\UserController::class, 'update_user_profile'])->name('update_profile');
});
