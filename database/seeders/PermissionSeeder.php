<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $crud_names = [
          'index',
          'create',
          'edit',
          'delete'
        ];
        $models = [
            'user',
            'permission'
        ];
        foreach ($models as $model){
            foreach ($crud_names as $crud_name ){
                Permission::updateOrCreate([
                    'name'=>$model.'_'.$crud_name
                ],[
                    'model_name'=>$model,
                    'name'=>$model.'_'.$crud_name
                ]);
            }
        }
    }
}
