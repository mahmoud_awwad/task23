<?php

return [
    ['section' => 'main', 'key' => 'logo', 'value' => ["ar" => 'images/technoraft-logo.png'], 'type' => 'file'],
    ['section' => 'main', 'key' => 'email', 'value' => ['ar' => 'info@technoraft.com'], 'type' => 'text'],
    ['section' => 'main', 'key' => 'phone', 'value' => ["ar" => '01012345678'], 'type' => 'text'],
    ['section' => 'main', 'key' => 'phone_2', 'value' => ["ar" => '0501234567'], 'type' => 'text'],
    ['section' => 'income', 'key' => 'charity_per_meter_unit', 'value' => ['ar' => 5], 'type' => 'number'],
    ['section' => 'income', 'key' => 'partners_withdraw_percentage', 'value' => ['ar' => 70], 'type' => 'number'],
    ['section' => 'main', 'key' => 'address', 'value' => ['ar' => 'ادخل العنوان'], 'type' => 'textarea'],
];
