@php
    $urls = array(
//        '0' => array(
//            'route' => route('users.index'),
//            'icon' => 'bi bi-person',
//            'name' => 'Users'
//        ),
//
//        '1' => array(
//            'route' => 'value4',
//            'icon' => 'value5',
//            'name' => 'Permission'
//        )
    );
@endphp
@foreach($urls as $key=>$url)
    <div class="menu-item">
        <a class="menu-link @if(request()->fullUrl() === $url['route']) active @endif" href="{{ $url['route'] }}">
            <span class="menu-bullet">
                <span class={{$url['icon']}}></span>
            </span>
            <span class="menu-title">{{__($url['name'])}}</span>
        </a>
    </div>
@endforeach
