<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" direction="{{ app()->getLocale() === 'ar' ? 'rtl' : 'ltr' }}"
      dir="{{ app()->getLocale() === 'ar' ? 'rtl' : 'ltr' }}"
      style="direction: {{ app()->getLocale() === 'ar' ? 'rtl' : 'ltr' }}">
<!--begin::Head-->
@include('layouts.head')
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="bg-body">
<section class="login-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-8 col-12 mx-auto">
                <div class="content">
                    <img alt="Logo" src="@yield('logo')" style="background-color: white"/>

                    <h1>{{ __('Welcome to your system') }}</h1>

                    <p>
                        {{ __('Managing your system will be easy with us') }}
                    </p>

                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</section>
<!--end::Root-->
<!--end::Main-->
<!--begin::Javascript-->
{{--<script>--}}
{{--    var hostUrl = "metronic/assets/";--}}
{{--</script>--}}
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{ asset('metronic/assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('metronic/assets/js/scripts.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--end::Javascript-->
</body>
<!--end::Body-->

</html>
