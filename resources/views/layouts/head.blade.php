<head>
    <title>{{ config('app.name') }} | @yield('title', 'Home')</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href={{ asset('site/assets/images/logo/icon.png') }} type="image/x-icon"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link rel="stylesheet"
          href="{{ asset('metronic/assets/plugins/custom/datatables/datatables.bundle.rtl.css') }}">
    <link href="{{ asset('metronic/assets/plugins/global/plugins.bundle.rtl.css') }}" rel="stylesheet"
          type="text/css" />

    {{-- <link href="{{ asset('metronic/assets/app.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('metronic/assets/design.css') }}" rel="stylesheet" type="text/css" />
    @if (app()->getLocale() === 'en')
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <!--end::Fonts-->
        <link href="{{ asset('metronic/assets/css/style.bundle.css') }}" rel="stylesheet"
              type="text/css" />
        <style>
            :root {
                --bs-font-sans-serif: Poppins, Helvetica, "sans-serif"
            }

            .v-application {
                font-family: var(--bs-font-sans-serif) !important;
            }
        </style>
    @else
        <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@700&amp;display=swap" rel="stylesheet">
        <link href="{{ asset('metronic/assets/css/style.bundle.rtl.css') }}" rel="stylesheet"
              type="text/css" />

        <style>
            :root {
                --bs-font-sans-serif: "Tajawal", Helvetica, "sans-serif"
            }

            body,
            html {
                font-family: var(--bs-font-sans-serif)
            }
        </style>
    @endif
    {{-- <link href="{{ asset('css/custom_styles.css') }}" rel="stylesheet" type="text/css"> --}}
    <link href="{{ asset('css/design.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    @stack('header')
    <!--end::Page Vendor Stylesheets-->
</head>
