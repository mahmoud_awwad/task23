@php
    $urls = array(
        '0' => array(
            'route' => route('users.index'),
            'icon' => 'bi bi-person',
            'permission' => 'user_index',
            'name' => 'Users'
        ),

        '1' => array(
            'route' => route('Roles.index'),
            'icon' => 'fa fa-lock',
            'permission' => 'permission_index',
            'name' => 'Permission'
        )
    );
@endphp
@foreach($urls as $key=>$url)
    @if(auth()->user()->hasPermission($url['permission']) || auth()->user()->id == "1")
        <div class="menu-item">
            <a class="menu-link @if(request()->fullUrl() === $url['route']) active @endif" href="{{ $url['route'] }}">
                <span class="menu-bullet">
                    <span class="{{$url['icon']}}"></span>
                </span>
                <span class="menu-title">{{__($url['name'])}}</span>
            </a>
        </div>
    @endif
@endforeach
