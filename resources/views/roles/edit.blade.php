@extends('layouts.master')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('Roles.update',$Role->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="name">{{__('Name')}}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',$Role->name) }}" required autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

            </div>
            <div class="row justify-content-center">
                <table class="table">
                    <thead class="thead-dark" style="background-color: #67c7fd !important;text-align: center">
                        <tr>
                            <th scope="col">{{__('Name')}}</th>
                            <th scope="col">{{__('Index')}}</th>
                            <th scope="col">{{__('Create')}}</th>
                            <th scope="col">{{__('Edit')}}</th>
                            <th scope="col">{{__('Delete')}}</th>
                        </tr>
                    </thead>
                    <tbody style="text-align: center">
                        @forelse($permissions as $permission)
                            <tr>
                                <th scope="row">{{__($permission)}}</th>
                                @foreach(\App\Models\Permission::where('model_name',$permission)->get() as $permission_name)
                                    <td><input type="checkbox" @checked($Role->permissions->contains($permission_name->id) ? 'checked' : '') name="permissions[]" value="{{ $permission_name->id }}"> {{--{{ $permission_name->name }}--}}</td>
                                @endforeach
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">
                                    <center>
                                        <h3>{{__('No permissions yet.')}}</h3>
                                    </center>
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <button type="submit" class="btn btn-primary"><span>{{__('Update role')}}</span></button>
        </form>

</div>
@endsection
