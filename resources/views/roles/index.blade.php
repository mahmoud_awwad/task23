@extends('layouts.master')

@section('content')
    <div class="container">
        @if(auth()->user()->hasPermission('permission_create') || auth()->user()->id == "1")
            <a href="{{route('Roles.create')}}" class="btn btn-primary"> <span>{{__('Create role')}}</span></a>
        @endif
        <div class="row justify-content-center">
            <table class="table">
                <thead class="thead-dark" style="background-color: #67c7fd !important;text-align: center">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{__('Name')}}</th>
                    <th colspan="2">{{__('Action')}}</th>
                </tr>
                </thead>
                <tbody style="text-align: center">
                @forelse($roles as $role)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$role->name}}</td>
                    @if( auth()->user()->type == "admin")
                        <td colspan="1">
                            <center>
                                @if(auth()->user()->hasPermission('permission_edit') || auth()->user()->id == "1")
                                    <a href="{{route('Roles.edit',$role->id)}}" class="btn btn-primary"><span>{{__('Edit')}}</span></a>
                                @endif
                                @if(auth()->user()->hasPermission('permission_delete') || auth()->user()->id == "1")
                                    <form action="{{ route('Roles.destroy', $role->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-primary btn-sm"><span>{{__('Delete Role')}}</span></button>
                                    </form>
                                @endif
                            </center>
                        </td>
                    @else
                        <td colspan="2">
                            <center>
                            </center>
                        </td>
                    @endif
                </tr>
                @empty
                    <tr>
                        <td colspan="5">
                            <center>
                                <h3>{{__('No roles yet.')}}</h3>
                            </center>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
