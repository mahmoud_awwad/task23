<x-ui::layout title="{{ __('Settings') }}" :breadcrumbs="[]">
    <div class="card">
        <div class="card-header border-0">
            <div class="card-title">
                {{ __('Settings') }} </div>
        </div>
        <div class="card-body">
            <form id="kt_modal_new_target_form" class="form indicator_form" method="POST"
                action="{{ route('dashboard.core.settings.update') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    @foreach ($settings as $key => $setting)
                        @if ($setting->type == 'textarea')
                            @if (isset($setting->value['en']))
                                <div class="form-group col-md-6 pt-7">
                                    <label><strong>{{ trans($setting['key']) }} Arabic </strong></label>
                                    <textarea name="settings[{{ $setting['key'] }}][ar]" rows="3" class="form-control" rows="10">{{ $setting->value['ar'] }}</textarea>
                                </div>
                                <div class="form-group col-md-6 pt-7">
                                    <label><strong>{{ trans($setting['key']) }} English</strong></label>
                                    <textarea name="settings[{{ $setting['key'] }}][en]" rows="3" class="form-control" rows="10">{{ $setting->value['en'] }}</textarea>
                                </div>
                            @else
                                <div class="form-group col-md-12 pt-7">
                                    <label><strong>{{ trans($setting['key']) }}</strong></label>
                                    <textarea name="settings[{{ $setting['key'] }}][ar]" rows="3" class="text-area-shape"
                                        rows="10">{{ $setting->value['ar'] }}</textarea>
                                </div>
                            @endif
                        @endif

                        @if ($setting->type !== 'textarea')
                            @if (isset($setting->value['en']))
                                <div class="form-group col-md-12 pt-7">
                                    <label><strong>{{ trans($setting['key']) }} Arabic </strong></label>
                                    <input type="{{ $setting->type }}" class="form-control"
                                        value="{{ $setting->value['ar'] }}"
                                        name="settings[{{ $setting['key'] }}][ar]">
                                </div>
                                <div class="form-group col-md-6 pt-7">
                                    <label><strong>{{ trans($setting['key']) }} English</strong></label>
                                    <input type="{{ $setting->type }}" class="form-control"
                                        value="{{ $setting->value['en'] }}"
                                        name="settings[{{ $setting['key'] }}][en]">
                                </div>
                            @else
                                @if ($setting->type == 'file')
                                    <div class="form-group col-md-2 pt-7">
                                        <img style="background-color: rgb(211, 207, 207); border-radius: 5%;"
                                            src="{{ asset($setting['value']['ar']) }}" width="100%" height="100%">
                                    </div>
                                    <div class="form-group col-md-4 pt-7 align-self-center">
                                        <label><strong>{{ trans($setting['key']) }}</strong></label>
                                        <input type="{{ $setting->type }}" class="form-control "
                                            value="{{ $setting->value['ar'] ?? '' }}"
                                            name="settings[{{ $setting['key'] }}][ar]">
                                    </div>
                                @else
                                    <div class="form-group col-md-6 pt-7">
                                        <label><strong>{{ trans($setting['key']) }}</strong></label>
                                        <input type="{{ $setting->type }}" class="form-control"
                                            value="{{ $setting->value['ar'] }}"
                                            name="settings[{{ $setting['key'] }}][ar]">
                                    </div>
                                @endif
                            @endif
                        @endif
                        <br>
                    @endforeach
                </div>
                <div class="col-xs-12 mt-5">
                    <button type="submit" class="btn btn-success mx-2 px-5">
                        <i class="fa fa-save"></i> {{ __('Save data') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</x-ui::layout>
