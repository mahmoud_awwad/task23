@extends('layouts.master')

@section('content')
    <div class="container"><form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="name">{{__('Name')}}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autofocus>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="email">{{__('Email')}}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="phone">{{__('Phone')}}</label>
                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required>
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="password">{{__('Password')}}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                    <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                          data-kt-password-meter-control="visibility">
                        <i class="bi bi-eye-slash fs-2"></i>
                        <i class="bi bi-eye fs-2 d-none"></i>
                    </span>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="password_confirmation">{{__('Confirm Password')}}</label>
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                    <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                          data-kt-password-meter-control="visibility">
                        <i class="bi bi-eye-slash fs-2"></i>
                        <i class="bi bi-eye fs-2 d-none"></i>
                    </span>
                </div>

                <div class="form-group col-md-4">
                    <label for="image">{{__('Image')}}</label>
                    <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                    @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                @if(auth()->user()->id == "1")
                    <div class="form-group col-md-4">
                        <label for="type">{{__('User Type')}}</label>
                        <select name="type" id="type" class="form-control @error('type') is-invalid @enderror">
                            <option value="user" @selected(old('type') == 'user')>{{__('User')}}</option>
                            <option value="admin" @selected(old('type') == 'admin')>{{__('Admin')}}</option>
                        </select>
                        @error('type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                @endif
                @if(auth()->user()->type == "admin")
                    <div class="form-group col-md-4 select_admin_type">
                        <label for="type">{{__('Permissions')}}</label>
                        <select name="role_id" id="type" class="form-control @error('role_id') is-invalid @enderror">
                            <option value="">{{__('Select role')}}</option>
                            @foreach($roles as $role)
                                <option value={{$role->id}} @selected(old('role_id') == $role->id)>{{$role->name}}</option>
                            @endforeach
                        </select>
                        @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                @endif

            </div>
            <button type="submit" class="btn btn-primary"><span>{{__('Create User')}}</span></button>
        </form>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function() {
                if($('#type').val() == 'user'){
                    $('.select_admin_type').hide();
                }else{
                    $('.select_admin_type').show();
                }
                $('#type').change(function() {
                    var selectedRole = $(this).val();

                    if (selectedRole === 'user') {
                        $('.select_admin_type').hide();
                    } else {
                        $('.select_admin_type').show();
                    }
                });
            });
        </script>
    @endpush
@endsection
