@extends('layouts.master')

@section('content')
    <div class="container">
        @if(auth()->user()->hasPermission('user_create') || auth()->user()->id == "1")
            <a href="{{route('users.create')}}" class="btn btn-primary"> <span>{{__('Create')}}</span></a>
        @endif
        <div class="row justify-content-center">
            <table class="table">
                <thead class="thead-dark" style="background-color: #67c7fd !important;text-align: center">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{__('Name')}}</th>
                    <th scope="col">{{__('Email')}}</th>
                    <th scope="col">{{__('Phone')}}</th>
                    <th colspan="2">{{__('Action')}}</th>
                </tr>
                </thead>
                <tbody style="text-align: center">
                @forelse($users as $user)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    @if(auth()->user()->id == $user->id || auth()->user()->type == "admin")
                        <td colspan="2">
                            <center>
                                @if(auth()->user()->hasPermission('user_edit') || auth()->user()->id == "1")
                                    <a href="{{route('users.edit',$user->id)}}" class="btn btn-primary"><span>{{__('Edit')}}</span></a>
                                @endif
                                @if(auth()->user()->hasPermission('user_delete') || auth()->user()->id == "1")
                                    <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-primary btn-sm"><span>{{__('Delete User')}}</span></button>
                                    </form>
                                @endif
                            </center>
                        </td>
                    @endif
                    <td colspan="2">
                        <center>

                        </center>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="5">
                            <center>
                                <h3>{{__('No data yet.')}}</h3>
                            </center>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
