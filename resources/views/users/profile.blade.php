@extends('layouts.master')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('update_profile',auth()->user()->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
            <div class="form-group col-md-4">
                <label for="name">{{__('Name')}}</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',auth()->user()->name) }}" required autofocus>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="email">{{__('Email')}}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',auth()->user()->email) }}" required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="phone">{{__('Phone')}}</label>
                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone',auth()->user()->phone) }}" required>
                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="password">{{__('Password')}}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" >
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="password_confirmation">{{__('Confirm Password')}}</label>
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" >
                </div>

                <div class="form-group col-md-4">
                    <label for="image">{{__('Image')}}</label>
                    <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                    @error('image')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    @if (auth()->user()->image)
                        <div>
                            <img src="{{ url('storage/' . auth()->user()->image) }}" alt="User Image" width="100">
                        </div>
                    @endif
                </div>
                @if(auth()->user()->type == "admin")
                    <div class="form-group col-md-4">
                        <label for="type">{{__('User Type')}}</label>
                        <select name="type" id="type" class="form-control @error('type') is-invalid @enderror">
                            <option value="user" @selected(auth()->user()->type === 'user')>{{__('User')}}</option>
                            <option value="admin" @selected(auth()->user()->type === 'admin')>{{__('Admin')}}</option>
                        </select>
                        @error('type')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                @endif
            </div>
            <button type="submit" class="btn btn-primary"><span>{{__('Update User')}}</span></button>
        </form>

    </div>
@endsection
